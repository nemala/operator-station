# Welcome to NeMALA Operator Station! #

This is a [NeMALA Alate](https://gitlab.com/nemala/alate) companion project.
The Alate agents call the server to let the operator know what they're up to, and to fetch operator commands. 

A [docker](https://docs.docker.com/get-started/) image is available (more information about the image
 [here](https://hub.docker.com/r/nemala/operator_station/)).
 To run from docker, see [Docker](#docker) instructions.
 We recommend running docker as a [non-root user](https://docs.docker.com/engine/install/linux-postinstall/).
 
 To install directly on a local host or server, see [Standalone](#standalone) instructions.

 Once installed, use the [UI and API](#api) to control your very own swarm!

## <a name="dependencies"></a>Dependencies ##
| Dependency                        | Minimal Version   | License
| ------------                      | ---------------   | -------
| [Django](https://docs.djangoproject.com/en/2.2/intro/install/)   | 1.11.5 | [Django License](https://github.com/django/django/blob/master/LICENSE)
| [GeographicLib](https://geographiclib.sourceforge.io/html/index.html) | 1.49 | [MIT/X11](https://geographiclib.sourceforge.io/html/LICENSE.txt)
| [Markdown](https://pypi.python.org/pypi/Markdown) | 2.6.9 | [BSD](https://opensource.org/licenses/BSD-3-Clause)
| [Django REST](http://www.django-rest-framework.org/) | 3.6.4 | [Django REST](http://www.django-rest-framework.org/#license)
| [Django Filter](https://github.com/carltongibson/django-filter) | 1.0.4 | [Django Filter](https://github.com/carltongibson/django-filter/blob/develop/LICENSE)
| [Django Extensions](https://github.com/django-extensions/django-extensions) | 1.9.1 | [MIT](https://github.com/django-extensions/django-extensions/blob/master/LICENSE) 

# <a name="docker"></a>Docker Deployment
1. Export the following environment variables:
    * NEMALA_OPERATOR_STATION_SECRET_KEY - make something up, long and weird.   
    * KEY_GOOGLE_MAPS - a google maps [JS API key](https://developers.google.com/maps/documentation/javascript/tutorial).
 
1. Create a docker bridge network:
	```console
	user@hostname:~/$ docker network create -d bridge alate_operator
    ```

1. Run and name the container, sharing the environment vairables and binding a usable port to the docker bridge network:
    ```console
    user@hostname:~/$ docker run --rm --env KEY_GOOGLE_MAPS --env NEMALA_OPERATOR_STATION_SECRET_KEY --name alate-server --network="alate_operator" -p 127.0.0.1:8000:8000 -it nemala/operator_station:latest
    ```

# <a name="standalone"></a>Standalone Deployment

1. Create a [virtual environment](https://docs.python.org/3/library/venv.html):
    ```console
    user@hostname:~/$ python3 -m venv AlateServer
    ```
   
1. Activate the virtual environment:
    ```console
    user@hostname:~/$ . AlateServer/bin/activate
    ```
   
1. Install Django to the virtual environment
    ```console
    (AlateServer)user@hostname:~/$ cd path_to_project_root/
    (AlateServer)user@hostname:~/path_to_project_root/$ pip3 install --no-cache-dir -r requirements.txt
    ```
   
1. Export the following environment variables:
    * NEMALA_OPERATOR_STATION_SECRET_KEY - make something up, long and weird.   
    * KEY_GOOGLE_MAPS - a google maps [JS API key](https://developers.google.com/maps/documentation/javascript/tutorial).
    
1. Django Migrate:
    ```console
        (AlateServer)user@hostname:~/path_to_project_root/$ python3 manage.py migrate
    ```
   
1. Run the server with the appropriate IP and port
    ```console
        (AlateServer)user@hostname:~/path_to_project_root/$ python3 manage.py runserver 127.0.0.1:8000
    ```    

# <a name="api"></a>Using the Operator Station
Now that everythin is set up, open a browser and go to the address that the server serves to, in these docs we used [127.0.0.1:8000](http://127.0.0.1:8000/).
If you click on the map window, a new beacon appears. There are also a bunch of buttons:
1. Takeoff
1. Land
1. Return to launch
1. Reset the agent table (generate some beacons, then press this button and see what happens to them).
1. Azimuth knob and switch - when the switch is on a "set direction" command is sent to the server with the knob's azimuth.

If you want to use a different UI than provided,
we expose an API at *your_chosen_url*/api/ and if you followed the deployment guide above,
you will find it at [http://127.0.0.1:8000/api/](http://127.0.0.1:8000/api/).
