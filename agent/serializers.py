# -*- coding: utf-8 -*-
from rest_framework import serializers
from agent.models import Operator, Agent


class AgentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agent
        fields = [ 'id', 'client_timestamp', 'server_timestamp', 'latitude', 'longitude',\
                   'altitude', 'azimuth', 'armed', 'bat_volt', 'gps_fix', 'gps_hdop',\
                   'mode', 'llc_message', 'llc_state', 'hlc_state', 'mc_state']

class OperatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operator
        fields = [ 'command', 'direction', 'server_timestamp' ]
