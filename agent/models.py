# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.

class Agent(models.Model):
    client_timestamp = models.DateTimeField('client time stamp')
    server_timestamp = models.DateTimeField('server reception time')
    latitude = models.FloatField()
    longitude = models.FloatField()
    altitude = models.FloatField()
    azimuth = models.FloatField()
    armed = models.BooleanField()
    bat_volt = models.FloatField('Battery Voltage')
    gps_fix = models.IntegerField()
    gps_hdop = models.FloatField()
    mode = models.CharField(max_length=20)
    llc_message = models.CharField(max_length=200)
    llc_state = models.CharField(max_length=20)
    hlc_state = models.CharField(max_length=20)
    mc_state = models.CharField(max_length=20)
    
    def __str__(self):
        return "Agent "+str(self.id)

DictCommand = ['None','Takeoff','Land','GoHome','SetDirection']

class Operator(models.Model):
    command = models.IntegerField()
    direction = models.FloatField('Azimuth in degrees')
    server_timestamp = models.DateTimeField('server reception time')
    
    def __str__(self):
        return DictCommand[self.command]
    
    def NoCommand(self):
        self.command = 0
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()
        
    def Takeoff(self):
        self.command = 1
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()
        
    def Land(self):
        self.command = 2
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()

    def RTL(self):
        self.command = 3
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()
        
    def SetDirection(self,direction):
        self.command = 4
        self.direction = direction
        self.server_timestamp = timezone.now()
        self.save()        
        
    def isRelevant(self):
        timedeltaFromLastCommand = timezone.now() - self.server_timestamp
        return ((timedeltaFromLastCommand.seconds < 3) and (0 <= timedeltaFromLastCommand.seconds))
