from django.conf.urls import url
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from . import views

app_name = 'agent'
urlpatterns = [
    path('api/', views.API.as_view()),
    path('api/takeoff/', views.Takeoff.as_view(), name='takeoff'),
    path('api/direction/', views.SetDirection.as_view(), name='direction'),
    path('api/land/', views.Land.as_view(), name='land'),
    path('api/rtl/', views.ReturnToLaunch.as_view(), name='rtl'),
    path('api/command/<int:pk>/', views.LatestCommand.as_view(), name='command'),
    path('api/list/', views.AgentList.as_view(), name='agent_list'),
    path('api/agent/<int:pk>/', views.AgentDetails.as_view(), name='details'),
    path('api/agent/new/', views.CreateAgent.as_view(), name='new_agent'),
    url(r'^agent/', views.agents, name='agents'),
    url(r'^ui/update/', views.update, name='update'),
    url(r'^ui/direction/', views.set_direction, name='set_direction'),
    url(r'^ui/reset/', views.reset, name='reset_agents'),
    url(r'^ui/beacon/', views.beacon, name='beacon'),
    url(r'^', views.users, name='users'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
