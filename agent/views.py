# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import datetime
import pytz
from math import sin, cos, pi

from geographiclib.geodesic import Geodesic

from django.utils import timezone
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect

from rest_framework import generics, renderers
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse

from agent.models import Agent, Operator
from agent.serializers import AgentSerializer, OperatorSerializer

no_id = 4294967295 # largest integer
visibilityDistance = 100000 #meters
alpha = 40  #degrees


#---------------------------------- Helper functions ---------------------------------

def detected_others(agent):
    for other in Agent.objects.exclude(pk=agent.id):
        g = Geodesic.WGS84.Inverse(agent.latitude, agent.longitude, other.latitude, other.longitude)
        if ((visibilityDistance > g['s12']) and (cos((g['azi1']*pi/180 - agent.azimuth)) > cos(alpha*pi/360))):
            return True
    return False;

def get_latest_operator():
    if (False == Operator.objects.exists()):
        user = Operator()
        user.NoCommand()
    else:
        user = Operator.objects.order_by('-server_timestamp')[0]
    return user

#--------------------------------- API ----------------------------------

class API(APIView):
    """
    NeMALA / Operator Station API
    """
    def get(self, request, format=None):
        reply = {
            'Takeoff': reverse('agent:takeoff', request=request, format=format),
            'Set Direction': reverse('agent:direction', request=request, format=format),
            'Land': reverse('agent:land', request=request, format=format),
            'Return to Launch': reverse('agent:rtl', request=request, format=format),
            'Last Operator Command': reverse('agent:command', args=('1',), request=request, format=format),
            'Agent List': reverse('agent:agent_list', request=request, format=format),
            'New Agent': reverse('agent:new_agent', request=request, format=format)
            }
        for agent in Agent.objects.order_by('id'):
            str_name = agent.__str__() + ' Details'
            reply[str_name] = reverse('agent:details', args=(agent.id,), request=request, format=format)
        return Response(reply)

class Takeoff(APIView):
    """
    Operator Command to all listening agents to perform a takeoff
    """
    def post(self, request, format=None):
        user = get_latest_operator()
        user.Takeoff()
        return Response(status=200)


class SetDirection(APIView):
    """
    Operator Command to all listening agents to move towards some direction \
    (direction x in degrees, 0 <= x < 360) for 3 seconds.\
    Example : {"direction": 64}
    """
    def post(self, request, format=None):
        reply = Response(status=200)
        direction = 0
        try:
            direction = float(request.data['direction'])
        except Exception as e:
            reply = Response(data=e.__str__(), status=400)
        else:
            if ( (0 <= direction) and (360 > direction) ):
                user = get_latest_operator()
                user.SetDirection(direction)
            else:
                reply = Response(data='Azimuth out of bounds.', status=400)
        return reply

class Land(APIView):
    """
    Operator Command to all listening agents to land
    """
    def post(self, request, format=None):
        user = get_latest_operator()
        user.Land()
        return Response(status=200)
    
class ReturnToLaunch(APIView):
    """
    Operator Command to all listening agents to return to launch
    """
    def post(self, request, format=None):
        user = get_latest_operator()
        user.RTL()
        return Response(status=200)

class LatestCommand(generics.RetrieveAPIView):
    """
    Latest Command Input
    """
    queryset = Operator.objects.all()
    serializer_class = OperatorSerializer

class AgentList(generics.ListAPIView):
    """
    A list of current active agents
    """
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer
    
class AgentDetails(generics.RetrieveUpdateDestroyAPIView):
    """
    Manipulate a specific agent or beacon
    """
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer
    
class CreateAgent(generics.CreateAPIView):
    """
    Create a new agent or beacon
    """
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer

#--------------------------------------------- UI -------------------------------------------

def users(request):
    agent_list = Agent.objects.order_by('id')
    context = {'agent_list': agent_list, 'google_maps_key': settings.GOOGLE_MAPS_API_KEY}
    return render(request, 'agent/operator.html', context)

def update(request):
    """
    Responds to a user's Get request with the latest Operator's command and Agent list.
    """
    agent_list = Agent.objects.order_by('id')
    context = {'server_time' : timezone.now() , 'agent_list': agent_list}
    return render(request, 'agent/update.html', context)

def set_direction(request):
    """
    Similar to update, just accepts a POST with a direction command.
    """
    reply = HttpResponse(status=405)
    if ('POST' == request.method):
        user = get_latest_operator()
        user.SetDirection(request.POST['direction'])
        reply = update(request)
    return reply

def reset(request):
    """
    Expects a POST request to remove all inactive agents and beacons
    """
    reply = HttpResponse(status=405)
    if ('POST' == request.method):
        Agent.objects.all().delete()
        reply = HttpResponseRedirect(reverse('agent:users'))
    return reply

def beacon(request):
    """
    Adds a phantom agent
    """
    reply = HttpResponse(status=405)
    if ('POST' == request.method):
        agent = Agent(
            client_timestamp = timezone.now(),
            server_timestamp = timezone.now(),
            latitude = float(request.POST['lat']),
            longitude = float(request.POST['lon']),
            altitude = 0,
            azimuth = 3.14,
            armed = 0,
            bat_volt = 0,
            gps_fix = 0,
            gps_hdop = 0,
            mode = "BEACON",
            llc_message = "this",
            llc_state = "is",
            hlc_state = "a",
            mc_state = "beacon"
        )
        agent.save()
        reply = HttpResponseRedirect(reverse('agent:users'))
    return reply

#--------------------------------------- Agent -----------------------------------------------

@api_view(['POST'])
def agents(request):
    """
    Responds to an agent's POST request with the latest Operator's command.
    """
    reply = HttpResponse(content='POST expected', status=405)
    if ('POST' == request.method):
        dictMessage = json.loads(request.POST['root'])
        
        #print(request.POST['root'])
        
        agentId = int(dictMessage['Message']['Header']['Topic'])
        unaware = datetime.datetime.strptime(dictMessage['Message']['Header']['TimeStamp'],"%Y-%m-%dT%H:%M:%S.%f")
        now_aware = unaware.replace(tzinfo=pytz.UTC)
        agent = Agent(  
                        client_timestamp = now_aware,
                        server_timestamp = timezone.now(),
                        latitude = float(dictMessage['Message']['Body']['Lat']),
                        longitude = float(dictMessage['Message']['Body']['Lon']),
                        altitude = float(dictMessage['Message']['Body']['Alt']),
                        azimuth = float(dictMessage['Message']['Body']['Yaw']),
                        armed = (1 == int(dictMessage['Message']['Body']['Armed'])),
                        bat_volt = float(dictMessage['Message']['Body']['Volt']),
                        gps_fix = int(dictMessage['Message']['Body']['GpsFix']),
                        gps_hdop = float(dictMessage['Message']['Body']['GpsHdop']),
                        mode = dictMessage['Message']['Body']['Mode'],
                        llc_message = dictMessage['Message']['Body']['LlcMessage'],
                        llc_state = dictMessage['Message']['Body']['LlcState'],
                        hlc_state = dictMessage['Message']['Body']['HlcState'],
                        mc_state = dictMessage['Message']['Body']['McState']
                    )
        
        if (no_id == agentId):
            agent.id = None
        else:
            agent.id = agentId
        
        agent.save()
        
        # Check if other agents are in this agent's field of view
        detected = detected_others(agent)
        
        user = get_latest_operator()

        if (False == user.isRelevant()):
                user.NoCommand()

        reply = {
            'id':  agent.id,
            'command': user.command,
            'direction': user.direction,
            'payload':
            {
                'detected': detected
            },
            'timestamp': user.server_timestamp.__str__()
            }
        #print json.dumps(reply)
        reply = HttpResponse(json.dumps(reply))
    return reply
