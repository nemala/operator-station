"use strict";
var map;
var markers = [];

//-------------------------------------------------------------

function SetupMap(latitude = 32.7768783,longitude = 35.023192)
{
	var beaconForm = document.getElementById("beacon");
	map = new google.maps.Map(document.getElementById('map'),
			{center: {lat: latitude, lng: longitude},zoom: 18,
			mapTypeId: google.maps.MapTypeId.HYBRID});
	
	// This event listener will submit a beacon to the server at the clicked location.
	map.addListener('click', function(event){
		beaconForm.querySelector("#beacon_lat").value = event.latLng.lat();
		beaconForm.querySelector("#beacon_lon").value = event.latLng.lng();
		beaconForm.submit();
		}
	);
}

//-------------------------------------------------------------

function UpdateMarkers(agents)
{
	deleteMarkers();
	for(var i = 0; i < agents.length; i++)
	{
		buttonElement = document.getElementById(agents[i].innerHTML);
		addMarker(extractPosition(agents[i]), Number(buttonElement.nextElementSibling.querySelector("#azi").innerHTML), buttonElement.style.backgroundColor);
	}
}

//-------------------------------------------------------------

function addMarker(locLatlng, azimuth, color)
{
	var icon = {
			path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
			scale: 5,
			rotation: (azimuth * 180) / Math.PI,
			fillColor: color,
			strokeColor: color,
			fillOpacity: 10
	}

	var marker = new google.maps.Marker(
    {
      position: locLatlng,
      map: map,
      title:"agent",
      icon: icon
    }
    );
	markers.push(marker);
}

//-------------------------------------------------------------

function extractPosition(elementAgent)
{
	var agentData;
	var latitude;
	var longitude;
	agentData = elementAgent.nextElementSibling;
	latitude = agentData.querySelector("#lat").innerHTML;
	longitude = agentData.querySelector("#lon").innerHTML;
	var result = new google.maps.LatLng(latitude,longitude);
	return result;
}

//-------------------------------------------------------------

// Sets the map on all markers in the array.
function setAllMap(map) 
{
  for (var i = 0; i < markers.length; i++) 
  {
	  markers[i].setMap(map);
  }
}

//-------------------------------------------------------------

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}

//-------------------------------------------------------------

// Shows any markers currently in the array.
function showMarkers() {
  setAllMap(map);
}

//-------------------------------------------------------------

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}
