// Create a new YUI instance and populate it with the required modules.
YUI().use('dial', function (Y) {
    // Dial is available and ready for use. Add implementation
    // code here.
	var dial = new Y.Dial({
        min:-36000,
        max: 36000,
        stepsPerRevolution:360,
        value: 0,
        strings: {label:'Azimuth:', resetStr:'Reset', tooltipHandle:'Drag to set value'},
    });
	var degsInCircle = 360
	dial.render("#direction_knob");
	
	function getInRange(inVal)
	{
		var retVal = inVal;
        if ( inVal < 0 )
        {
        	retVal = (inVal + degsInCircle * 10 ) % degsInCircle;
        }
        else if ( inVal > degsInCircle )
        {
        	retVal = inVal % degsInCircle;
        }
		return retVal;
	}
	
    // Function to update the dial value so it will stay in range.
    function updateInput( e )
    {
    	e.newVal = getInRange(e.newVal)
        // avoid recursion
        dial.detach("valueChange", updateInput)
        dial.set( "value" );
        dial.on( "valueChange", updateInput);
    }
    
    dial.on( "valueChange", updateInput);
    
    // Function to update the value field.
    function updateValue( e )
    {
    	if ( isNaN(e.newVal) )
    	{
    		return;
    	}
    	var val = getInRange(e.newVal);
    	this.set( "value", val );
    }
    
    var valueField = Y.one( "#direction_value" );
    dial.on( "valueChange", updateValue, valueField );
    
    // Initialize the valueField
    valueField.set('value', dial.get('value'));
});
