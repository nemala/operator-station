function MakeCollapsible(elem)
{
	elem.addEventListener("click", function(){
		this.classList.toggle("active");
		this.classList.toggle("full_width");
	    var content = this.nextElementSibling;
	    if (content.style.display === "block")
	    {
	        content.style.display = "none";
	    }
	    else
	    {
	        content.style.display = "block";
	    }
	});
}
