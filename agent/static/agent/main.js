function ColorButtonByStatus(buttonElement,serverTime)
{
	llc_state = buttonElement.nextElementSibling.querySelector("#llc_state").innerHTML;
	hlc_state = buttonElement.nextElementSibling.querySelector("#hlc_state").innerHTML;
	mc_state = buttonElement.nextElementSibling.querySelector("#mc_state").innerHTML;
	lastHeardOf = serverTime - Date.parse(buttonElement.nextElementSibling.querySelector("#server_timestamp").innerHTML)
	//alert(lastHeardOf);

	buttonElement.style.color = "black";
	if ("beacon" == mc_state)
	{
		buttonElement.style.backgroundColor = "black";
	}
	else if (
			(
					("STANDBY" === llc_state) ||
					("ACTIVE" === llc_state)
			) &&
			(
					("Ready" === hlc_state) ||
					("Takeoff" === hlc_state) ||
					("GainingAltitude" === hlc_state) ||
					("Airborne" === hlc_state) ||
					("Landing" === hlc_state)					
			) &&
			(
					("Standby" === mc_state) ||
					("Takingoff" === mc_state) ||
					("Mission" === mc_state) ||
					("Landing" === mc_state)
			)
		)
	{
		buttonElement.style.backgroundColor = "green";
	}
	else if (
			(
					("UNINIT" === llc_state) ||
					("BOOT" === llc_state) ||
					("CALIBRATING" === llc_state)
			) ||
			(
					("None" === hlc_state) ||
					("Init" === hlc_state) ||
					("WaitingForMc" === hlc_state) ||
					("WaitingForLlc" === hlc_state) ||
					("Manual" === hlc_state) ||
					("Return to Launch" === hlc_state) ||
					("Low Battery" === hlc_state)					
			) ||
			(
					("None" === mc_state) ||
					("Init" === mc_state) ||
					("Manual" === mc_state) ||
					("Return to Launch" === mc_state)
			)
		)
	{
		buttonElement.style.backgroundColor = "yellow";
	}
	else if (
			(
					("CRITICAL" === llc_state) ||
					("EMERGENCY" === llc_state) ||
					("POWEROFF" === llc_state)
			) ||
			(
					("LLC Down" === hlc_state)	
			) ||
			(
					("Error!" === mc_state)
			)
		)
	{
		buttonElement.style.backgroundColor = "red";
	}
	if (2000 < lastHeardOf)
	{
		buttonElement.style.color = buttonElement.style.backgroundColor;
		buttonElement.style.backgroundColor = "blue";
	}
}

function UpdateStatus(response)
{
	//alert(response);
	var parser, xmlDoc;
	parser = new DOMParser();
	xmlDoc = parser.parseFromString(response,"text/xml");
	var agents = xmlDoc.getElementsByTagName("button");
	var serverTime = Date.parse(xmlDoc.getElementById("server_time").innerHTML);
	for(var i = 0; i < agents.length; i++)
	{
	    var incomingAgent = agents[i];
	    //alert(incomingAgent.innerHTML);
	    buttonElement = document.getElementById(incomingAgent.innerHTML);
	    if (buttonElement)
	    {
	    	//alert("Found!");
	    	buttonElement.nextElementSibling.innerHTML = incomingAgent.nextElementSibling.innerHTML;
	    	ColorButtonByStatus(buttonElement,serverTime)
	    }
	    else
	    {
	    	//alert("Not Found!");
	    	window.location.reload(true);
	    }
	}
	UpdateMarkers(agents);
}
