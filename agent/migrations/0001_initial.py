# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-04-25 12:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Agent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uid', models.IntegerField(verbose_name='Agent ID')),
                ('client_timestamp', models.DateTimeField(verbose_name='client time stamp')),
                ('server_timestamp', models.DateTimeField(verbose_name='server reception time')),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('altitude', models.FloatField()),
                ('azimuth', models.FloatField()),
                ('armed', models.BooleanField()),
                ('bat_volt', models.FloatField(verbose_name='Battery Voltage')),
                ('gps_fix', models.IntegerField()),
                ('gps_hdop', models.FloatField()),
                ('mode', models.CharField(max_length=20)),
                ('llc_message', models.CharField(max_length=200)),
                ('llc_state', models.CharField(max_length=20)),
                ('hlc_state', models.CharField(max_length=20)),
                ('mc_state', models.CharField(max_length=20)),
            ],
        ),
    ]
